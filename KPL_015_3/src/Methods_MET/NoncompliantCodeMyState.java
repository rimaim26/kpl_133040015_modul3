/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods_MET;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class NoncompliantCodeMyState {

    private Object myState = null;
// Sets some internal state in the library

    void setState(Object state) {
        myState = state;
    }
// Performs some action using the state passed earlier

    void useState() {
// Perform some action here
    }
}
