/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Methods_MET;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CompliantSolution_NetworkHandler {

    public final class NetworkHandler {

        private final ExecutorService executor;

        NetworkHandler(int poolSize) {
            this.executor = Executors.newFixedThreadPool(poolSize);
        }

        public void startThreads() {
            for (int i = 0; i < 3; i++) {
                executor.execute(new HandleRequest());
            }
        }

        public void shutdownPool() {
            executor.shutdown();
        }

        public static void main(String[] args) {
            NetworkHandler nh = new NetworkHandler(3);
            nh.startThreads();
            nh.shutdownPool();
        }
    }
}
