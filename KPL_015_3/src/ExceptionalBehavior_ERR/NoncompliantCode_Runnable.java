/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExceptionalBehavior_ERR;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class NoncompliantCode_Runnable {

    class Foo implements Runnable {

        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
// Ignore
            }
        }
    }
}
