/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExceptionalBehavior_ERR;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CompliantSolution_ExceptionExample1 {

    class ExceptionExample {

        public static void main(String[] args) {
            FileInputStream fis = null;
            try {
                switch (Integer.valueOf(args[0])) {
                    case 1:
                        fis = new FileInputStream("c:\\homepath\\file1");
                        break;
                    case 2:
                        fis = new FileInputStream("c:\\homepath\\file2");
                        break;
//...
                    default:
                        System.out.println("Invalid option");
                        break;
                }
            } catch (Throwable t) {
                MyExceptionReporter.report(t); // Sanitize
            }
        }
    }
}
