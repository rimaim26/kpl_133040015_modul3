/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExceptionalBehavior_ERR;
/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CompliantSolution_Runnable {

    class Foo implements Runnable {

        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt(); // Reset interrupted status
            }
        }
    }
}
