/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExceptionalBehavior_ERR;

/**
 *
 * @author rivaldhy Irmwan
 * latihan 
 */
public class CompliantSolution_ExceptionExample {

    class ExceptionExample {

        public static void main(String[] args) {
            File file = null;
            try {
                file = new File(System.getenv("APPDATA")
                        + args[0]).getCanonicalFile();
                if (!file.getPath().startsWith("c:\\homepath")) {
                    System.out.println("Invalid file");
                    return;
                }
            } catch (IOException x) {
                System.out.println("Invalid file");
                return;
            }
            try {
                FileInputStream fis = new FileInputStream(file);
            }
        }
        catch (FileNotFoundException x

        
            ) {
System.out.println("Invalid file");
            return;
        }
    }
}
